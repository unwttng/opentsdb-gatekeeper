module.exports = {
  LISTEN_PORT: process.env.LISTEN_PORT || 8000,
  OPENTSDB_HOSTNAME: process.env.OPENTSDB_HOSTNAME || 'localhost',
  OPENTSDB_PORT: process.env.OPENTSDB_PORT || 4242,
  WHITELIST: process.env.WHITELIST ? process.env.WHITELIST.split(',').map(pair => {
    const [ prefix, secret, ] = pair.split(':')
    return { prefix, secret, }
  }) : [],
}
