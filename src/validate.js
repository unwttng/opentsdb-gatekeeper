const _ = require('lodash')

function validateAgainstWhitelist({ metric, value, timestamp, tags, secret, }, whitelist) {
  const whitelistEntry = _.find(whitelist, entry => _.startsWith(metric, entry.prefix))
  if (!whitelistEntry) {
    throw new Error(`No whitelist entry found with valid prefix for metric ${metric}`)
  }
  if (whitelistEntry.secret !== secret) {
    // TODO obviously this security mechanism isn't up to much
    throw new Error(`Secret for whitelist entry ${whitelistEntry.prefix} does not match supplied secret`)
  }
}

module.exports = { validateAgainstWhitelist, }
