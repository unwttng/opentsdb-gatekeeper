const express = require('express')
const bodyParser = require('body-parser')
const request = require('request-promise-native')
const { log, } = require('peasy-log')

const cfg = require('./cfg')
const whitelist = cfg.WHITELIST
const { validateAgainstWhitelist, } = require('./src/validate')

const app = express()

app.use(bodyParser.json())

app.post('/metric', async (req, res) => {
  const { metric, value, timestamp, tags, secret, } = req.body
  try {
    validateAgainstWhitelist({ metric, value, timestamp, tags, secret, }, whitelist)
  } catch(e) {
    log(`Request failed validation against whitelist: ${e}`)
    return res.status(400).end()
  }
  log(`~~Validated request~~ against whitelist, forwarding to OpenTSDB: **${metric} = ${value} (${timestamp}) ${JSON.stringify(tags)}**`)
  request.post(`http://${cfg.OPENTSDB_HOSTNAME}:${cfg.OPENTSDB_PORT}/api/put?details`, {
    json: { metric, value, timestamp, tags, },
  }).catch(e => log(`Failed forwarding request to OpenTSDB: ${e}`))
  // Deliberately didn't await the last line, we'll log errors but not tell the client
  res.status(200).end()
})

app.listen(cfg.LISTEN_PORT, () => {
  log(`~~Listening~~ on port **${cfg.LISTEN_PORT}**`)
})
