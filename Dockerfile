FROM node:8.1

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get autoremove -y

RUN useradd -ms /bin/bash gatekeeper
RUN mkdir -p /home/gatekeeper/opentsdb-gatekeeper
WORKDIR /home/gatekeeper/opentsdb-gatekeeper

COPY cfg.js package.json package-lock.json server.js /home/gatekeeper/opentsdb-gatekeeper/
COPY src/ /home/gatekeeper/opentsdb-gatekeeper/src/

RUN chown -R gatekeeper:gatekeeper .

USER gatekeeper

RUN npm install

ENTRYPOINT [ "node", "server.js" ]
